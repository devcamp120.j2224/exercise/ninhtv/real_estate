package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.CMasterLayout;
import org.springframework.stereotype.Repository;

@Repository
public interface IMasterLayoutRepository extends JpaRepository<CMasterLayout, Integer> {

}
