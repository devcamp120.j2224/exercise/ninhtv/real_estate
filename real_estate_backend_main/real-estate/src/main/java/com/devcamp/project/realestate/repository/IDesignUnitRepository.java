package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.project.realestate.model.CDesignUnit;

@Repository
public interface IDesignUnitRepository extends JpaRepository<CDesignUnit, Integer> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM `design_unit` WHERE `id` = :designUnitId ;", nativeQuery = true)
    Object deleteDesignUnitById(@Param("designUnitId") int designUnitId);
}
