package com.devcamp.project.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.*;
import org.springframework.stereotype.Repository;

@Repository
public interface IDistrictRepository extends JpaRepository<CDistrict, Integer> {

    @Query(value = "SELECT * FROM `district` WHERE `_province_id` = :provinceId ;", nativeQuery = true)
    List<CDistrict> getDistrictListByProvinceId(@Param("provinceId") Integer provinceId);
}
