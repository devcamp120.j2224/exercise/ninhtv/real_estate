package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.DistrictService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DistrictController {
    @Autowired
    IProvinceRepository pProvinceRepository;

    @Autowired
    IDistrictRepository pDistrictRepository;

    @Autowired
    DistrictService pDistrictService;

    @GetMapping("/districts")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getDistrictsList() {
        if (!pDistrictService.getDistrictList().isEmpty()) {
            return new ResponseEntity<>(pDistrictService.getDistrictList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/districts/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getDistrictById(@PathVariable Integer districtId) {
        if (pDistrictRepository.findById(districtId).isPresent()) {
            return new ResponseEntity<>(pDistrictRepository.findById(districtId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/districts/province/{provinceId}")
    public ResponseEntity<Object> getDistrictListByProvinceId(@PathVariable Integer provinceId) {
        if (!pDistrictRepository.getDistrictListByProvinceId(provinceId).isEmpty()) {
            return new ResponseEntity<>(pDistrictRepository.getDistrictListByProvinceId(provinceId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/districts/{provinceId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createDistrict(@PathVariable("provinceId") Integer provinceId,
            @RequestBody CDistrict cDistrict) {
        try {
            Optional<CProvince> provinceData = pProvinceRepository.findById(provinceId);
            if (provinceData.isPresent()) {
                return new ResponseEntity<>(pDistrictService.createDistrictObj(cDistrict, provinceData),
                        HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified District: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/districts/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateDistrict(@PathVariable("districtId") Integer districtId,
            @RequestBody CDistrict cDistrict) {
        try {
            Optional<CDistrict> districtData = pDistrictRepository.findById(districtId);
            if (districtData.isPresent()) {
                return new ResponseEntity<>(pDistrictService.updateDistrictObj(cDistrict, districtData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified District: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("District NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/districts/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable Integer districtId) {
        try {
            pDistrictRepository.deleteById(districtId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
