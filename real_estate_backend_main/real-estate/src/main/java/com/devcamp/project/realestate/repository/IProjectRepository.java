package com.devcamp.project.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.CProject;
import org.springframework.stereotype.Repository;

@Repository
public interface IProjectRepository extends JpaRepository<CProject, Integer> {

    @Query(value = "SELECT * FROM `project`", nativeQuery = true)
    List<CProject> getListOfProject(Pageable pageable);

    @Query(value = "SELECT * FROM `project` WHERE `_province_id` = :provinceId AND `_district_id` = :districtId LIMIT 6 OFFSET 0 ;", nativeQuery = true)
    List<CProject> getProjectByProvinceIdAndDistrictId(@Param("provinceId") Integer provinceId,
            @Param("districtId") Integer districtId);

}
