package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.DesignUnitService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DesignUnitController {
    @Autowired
    IDesignUnitRepository pDesignUnitRepository;

    @Autowired
    DesignUnitService pDesignUnitService;

    @GetMapping("/designUnits")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getDesignUnitList() {
        if (!pDesignUnitService.getAllDesignUnit().isEmpty()) {
            return new ResponseEntity<>(pDesignUnitService.getAllDesignUnit(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/designUnits/{designUnitId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getDesignUnitById(@PathVariable Integer designUnitId) {
        if (pDesignUnitRepository.findById(designUnitId).isPresent()) {
            return new ResponseEntity<>(pDesignUnitRepository.findById(designUnitId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/designUnits")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createDesignUnit(@RequestBody CDesignUnit cDesignUnit) {
        try {
            return new ResponseEntity<>(pDesignUnitService.createDesignUnitObj(cDesignUnit), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Design Unit: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/designUnits/{designUnitId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable("designUnitId") Integer designUnitId,
            @RequestBody CDesignUnit cDesignUnit) {
        try {
            Optional<CDesignUnit> designUnitData = pDesignUnitRepository.findById(designUnitId);
            if (designUnitData.isPresent()) {
                return new ResponseEntity<>(pDesignUnitService.updateDesignUnitObj(cDesignUnit, designUnitData),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Design Unit: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Design Unit NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/designUnits/{designUnitId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteDesignUnitById(@PathVariable int designUnitId) {
        try {
            pDesignUnitRepository.deleteDesignUnitById(designUnitId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
