package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.entity.Role;
import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class EmployeeController {

    @Autowired
    RoleRepository pRoleRepository;

    @Autowired
    IEmployeeRepository pEmployeeRepository;

    @Autowired
    EmployeeService pEmployeeService;

    @GetMapping("/employees")
    public ResponseEntity<Object> getEmployeeList() {
        if (!pEmployeeRepository.getListAllEmployee().isEmpty()) {
            return new ResponseEntity<>(pEmployeeRepository.getListAllEmployee(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/employees/{employeeId}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable Integer employeeId) {
        if (pEmployeeRepository.findById(employeeId).isPresent()) {
            return new ResponseEntity<>(pEmployeeRepository.findById(employeeId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/employees")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createEmployee(@RequestBody CEmployees cEmployees) {
        try {
            return new ResponseEntity<>(pEmployeeService.createEmployeeObj(cEmployees), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/employees/{employeeId}/role/{roleId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateEmployee(@PathVariable("employeeId") Integer employeeId,
            @PathVariable("roleId") Integer roleId, @RequestBody CEmployees cEmployees) {
        try {
            Optional<CEmployees> employeeData = pEmployeeRepository.findById(employeeId);
            Optional<Role> roleData = pRoleRepository.findById(roleId);
            if (employeeData.isPresent() && roleData.isPresent()) {
                return new ResponseEntity<>(pEmployeeService.updateEmployeeObj(cEmployees, employeeData, roleData),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Employee: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Employee NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/employees/{employeeId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable int employeeId) {
        try {
            pEmployeeRepository.deleteById(employeeId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
