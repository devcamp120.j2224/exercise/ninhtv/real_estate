package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.CLocation;
import org.springframework.stereotype.Repository;

@Repository
public interface ILocationRepository extends JpaRepository<CLocation, Integer>  {
    
}
