package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IConstructionContractorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConstructionContractorService {
    @Autowired
    IConstructionContractorRepository pConstructionContractorRepository;

    public List<CConstructionContractor> getConstructionContractorsList() {
        List<CConstructionContractor> constructionContractorList = new ArrayList<CConstructionContractor>();
        pConstructionContractorRepository.findAll().forEach(constructionContractorList::add);
        return constructionContractorList;
    }

    public Object createConstructionContractorObj(CConstructionContractor cConstructionContractor) {

        CConstructionContractor newConstructionContractor = new CConstructionContractor();
        newConstructionContractor.setName(cConstructionContractor.getName());
        newConstructionContractor.setDescription(cConstructionContractor.getDescription());
        newConstructionContractor.setProjects(cConstructionContractor.getProjects());
        newConstructionContractor.setAddress(cConstructionContractor.getAddress());
        newConstructionContractor.setPhone(cConstructionContractor.getPhone());
        newConstructionContractor.setPhone2(cConstructionContractor.getPhone2());
        newConstructionContractor.setFax(cConstructionContractor.getFax());
        newConstructionContractor.setEmail(cConstructionContractor.getEmail());
        newConstructionContractor.setWebsite(cConstructionContractor.getWebsite());
        newConstructionContractor.setNote(cConstructionContractor.getNote());

        CConstructionContractor savedConstructionContractor = pConstructionContractorRepository
                .save(newConstructionContractor);
        return savedConstructionContractor;
    }

    public Object updateConstructionContractorObj(CConstructionContractor cConstructionContractor,
            Optional<CConstructionContractor> cConstructionContractorData) {

        CConstructionContractor newConstructionContractor = cConstructionContractorData.get();
        newConstructionContractor.setName(cConstructionContractor.getName());
        newConstructionContractor.setDescription(cConstructionContractor.getDescription());
        newConstructionContractor.setProjects(cConstructionContractor.getProjects());
        newConstructionContractor.setAddress(cConstructionContractor.getAddress());
        newConstructionContractor.setPhone(cConstructionContractor.getPhone());
        newConstructionContractor.setPhone2(cConstructionContractor.getPhone2());
        newConstructionContractor.setFax(cConstructionContractor.getFax());
        newConstructionContractor.setEmail(cConstructionContractor.getEmail());
        newConstructionContractor.setWebsite(cConstructionContractor.getWebsite());
        newConstructionContractor.setNote(cConstructionContractor.getNote());

        CConstructionContractor savedConstructionContractor = pConstructionContractorRepository
                .save(newConstructionContractor);
        return savedConstructionContractor;
    }
}
