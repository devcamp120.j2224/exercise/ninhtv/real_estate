package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.RegionLinkService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class RegionLinkController {
    @Autowired
    IRegionLinkRepository pRegionLinkRepository;

    @Autowired
    RegionLinkService pRegionLinkService;

    @GetMapping("/regionLinks")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getRegionLinkList() {
        if (!pRegionLinkService.getRegionLinkList().isEmpty()) {
            return new ResponseEntity<>(pRegionLinkService.getRegionLinkList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/regionLinks/{regionLinkId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getRegionLinkById(@PathVariable Integer regionLinkId) {
        if (pRegionLinkRepository.findById(regionLinkId).isPresent()) {
            return new ResponseEntity<>(pRegionLinkRepository.findById(regionLinkId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/regionLinks")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createRegionLink(@RequestBody CRegionLink cRegionLink) {
        try {
            return new ResponseEntity<>(pRegionLinkService.createRegionLinkObj(cRegionLink), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified RegionLink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/regionLinks/{regionLinkId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateRegionLink(@PathVariable("regionLinkId") Integer regionLinkId,
            @RequestBody CRegionLink cRegionLink) {
        try {
            Optional<CRegionLink> regionLinkData = pRegionLinkRepository.findById(regionLinkId);
            if (regionLinkData.isPresent()) {
                return new ResponseEntity<>(pRegionLinkService.updateRegionLinkObj(cRegionLink, regionLinkData),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified RegionLink: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/regionLinks/{regionLinkId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteRegionLinkById(@PathVariable("regionLinkId") int regionLinkId) {
        try {
            pRegionLinkRepository.deleteById(regionLinkId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
