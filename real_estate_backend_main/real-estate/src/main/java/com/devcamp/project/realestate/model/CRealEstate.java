package com.devcamp.project.realestate.model;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "realestate")
public class CRealEstate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "type")
    private Integer type;

    @Column(name = "request")
    private Integer request;

    @ManyToOne
    private CProject project;

    @ManyToOne
    @JsonIgnore
    private CProvince province;

    @ManyToOne
    private CDistrict district;

    @ManyToOne
    @JsonIgnore
    private CWard wards;

    @ManyToOne
    @JsonIgnore
    private CStreet street;

    @Column(name = "address", length = 2000)
    private String address;

    @ManyToOne
    private CCustomer customer;

    @Column(name = "price", columnDefinition = "bigint")
    private Integer price;

    @Column(name = "price_min")
    private Integer priceMin;

    @Column(name = "price_time")
    private Integer priceTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create")
    private Date dateCreate;

    @Column(name = "acreage", columnDefinition = "Decimal(12,2)")
    private Double acreage;

    @Column(name = "direction")
    private Integer direction;

    @Column(name = "total_floors")
    private Integer totalFloors;

    @Column(name = "number_floors")
    private Integer numberFloors;

    @Column(name = "bath")
    private Integer bath;

    @Column(name = "apart_code", length = 10)
    private String apartCode;

    @Column(name = "wall_area", columnDefinition = "Decimal(12,2)")
    private Double wallArea;

    @Column(name = "bedroom")
    private Integer bedroom;

    @Column(name = "balcony")
    private Integer balcony;

    @Column(name = "landscape_view")
    private String landscapeView;

    @Column(name = "apart_loca")
    private Integer apartLoca;

    @Column(name = "apart_type")
    private Integer apartType;

    @Column(name = "furniture_type")
    private Integer furnitureType;

    @Column(name = "price_rent")
    private Integer priceRent;

    @Column(name = "return_rate")
    private Double returnRate;

    @Column(name = "legal_doc")
    private Integer legalDoc;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "width_y")
    private Integer widthY;

    @Column(name = "long_x")
    private Integer longX;

    @Column(name = "street_house")
    private Integer streetHouse;

    @Column(name = "FSBO")
    private Integer FSBO;

    @Column(name = "view_num")
    private Integer viewNum;

    @ManyToOne
    private CEmployees employee;

    @Column(name = "update_by")
    private Integer updateBy;

    @Column(name = "shape", length = 200)
    private String shape;

    @Column(name = "distance2facade")
    private Integer distance2facade;

    @Column(name = "adjacent_facade_num")
    private Integer adjacent_facade_num;

    @Column(name = "adjacent_road")
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private Integer alley_min_width;

    @Column(name = "adjacent_alley_min_width")
    private Integer adjacent_alley_min_width;

    @Column(name = "factor")
    private Integer factor;

    @Column(name = "structure", length = 2000)
    private String structure;

    @Column(name = "DTSXD")
    private Integer DTSXD;

    @Column(name = "CLCL")
    private Integer CLCL;

    @Column(name = "CTXD_price")
    private Integer CTXD_price;

    @Column(name = "CTXD_value")
    private Integer CTXD_value;

    @Column(name = "photo", length = 2000)
    private String photo;

    @Column(name = "_lat")
    private Double lat;

    @Column(name = "_lng")
    private Double lng;

    @Column(name = "status", columnDefinition = "enum('Y','N')")
    private String status;

    public CRealEstate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public CProject getProject() {
        return project;
    }

    public void setProject(CProject project) {
        this.project = project;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWards() {
        return wards;
    }

    public void setWards(CWard wards) {
        this.wards = wards;
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Integer priceMin) {
        this.priceMin = priceMin;
    }

    public Integer getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Integer priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getBath() {
        return bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public Integer getBedroom() {
        return bedroom;
    }

    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(Integer totalFloors) {
        this.totalFloors = totalFloors;
    }

    public Integer getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(Integer numberFloors) {
        this.numberFloors = numberFloors;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public Double getWallArea() {
        return wallArea;
    }

    public void setWallArea(Double wallArea) {
        this.wallArea = wallArea;
    }

    public Integer getBalcony() {
        return balcony;
    }

    public void setBalcony(Integer balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public Integer getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(Integer apartLoca) {
        this.apartLoca = apartLoca;
    }

    public Integer getApartType() {
        return apartType;
    }

    public void setApartType(Integer apartType) {
        this.apartType = apartType;
    }

    public Integer getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(Integer furnitureType) {
        this.furnitureType = furnitureType;
    }

    public Integer getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(Integer priceRent) {
        this.priceRent = priceRent;
    }

    public Double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(Double returnRate) {
        this.returnRate = returnRate;
    }

    public Integer getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(Integer legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWidthY() {
        return widthY;
    }

    public void setWidthY(Integer widthY) {
        this.widthY = widthY;
    }

    public Integer getLongX() {
        return longX;
    }

    public void setLongX(Integer longX) {
        this.longX = longX;
    }

    public Integer getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(Integer streetHouse) {
        this.streetHouse = streetHouse;
    }

    public Integer getFSBO() {
        return FSBO;
    }

    public void setFSBO(Integer fSBO) {
        FSBO = fSBO;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public CEmployees getEmployee() {
        return employee;
    }

    public void setEmployee(CEmployees employee) {
        this.employee = employee;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Integer getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(Integer distance2facade) {
        this.distance2facade = distance2facade;
    }

    public Integer getAdjacent_facade_num() {
        return adjacent_facade_num;
    }

    public void setAdjacent_facade_num(Integer adjacent_facade_num) {
        this.adjacent_facade_num = adjacent_facade_num;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public Integer getAlley_min_width() {
        return alley_min_width;
    }

    public void setAlley_min_width(Integer alley_min_width) {
        this.alley_min_width = alley_min_width;
    }

    public Integer getAdjacent_alley_min_width() {
        return adjacent_alley_min_width;
    }

    public void setAdjacent_alley_min_width(Integer adjacent_alley_min_width) {
        this.adjacent_alley_min_width = adjacent_alley_min_width;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Integer getDTSXD() {
        return DTSXD;
    }

    public void setDTSXD(Integer dTSXD) {
        DTSXD = dTSXD;
    }

    public Integer getCLCL() {
        return CLCL;
    }

    public void setCLCL(Integer cLCL) {
        CLCL = cLCL;
    }

    public Integer getCTXD_price() {
        return CTXD_price;
    }

    public void setCTXD_price(Integer cTXD_price) {
        CTXD_price = cTXD_price;
    }

    public Integer getCTXD_value() {
        return CTXD_value;
    }

    public void setCTXD_value(Integer cTXD_value) {
        CTXD_value = cTXD_value;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
