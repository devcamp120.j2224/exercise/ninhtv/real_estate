package com.devcamp.project.realestate.model;

import javax.persistence.*;

@Entity
@Table(name = "address_map")
public class CAddressMap {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "address", nullable = false, length = 5000)
    private String address;

    @Column(name = "_lat", nullable = false)
    private double lat;

    @Column(name = "_lng", nullable = false)
    private double lng;

    public CAddressMap() {
    }

    public CAddressMap(int id, String address, double lat, double lng) {
        this.id = id;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

}
