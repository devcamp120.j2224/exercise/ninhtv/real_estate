package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class StreetController {
    @Autowired
    IProvinceRepository pProvinceRepository;

    @Autowired
    IDistrictRepository pDistrictRepository;

    @Autowired
    IStreetRepository pStreetRepository;

    @Autowired
    StreetService pStreetService;

    @GetMapping("/streets")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getStreetsList() {
        if (!pStreetRepository.getStreetList().isEmpty()) {
            return new ResponseEntity<>(pStreetRepository.getStreetList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/streets/{streetId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getStreetById(@PathVariable Integer streetId) {
        if (pStreetRepository.findById(streetId).isPresent()) {
            return new ResponseEntity<>(pStreetRepository.findById(streetId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/streets/district/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> getStreetListByDistrictId(@PathVariable Integer districtId) {
        if (!pStreetRepository.getStreetListByDistrictId(districtId).isEmpty()) {
            return new ResponseEntity<>(pStreetRepository.getStreetListByDistrictId(districtId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/streets/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createStreet(@PathVariable("districtId") int districtId,
            @RequestBody CStreet cStreet) {
        try {
            Optional<CDistrict> districtData = pDistrictRepository.findById(districtId);
            if (districtData.isPresent()) {
                return new ResponseEntity<>(pStreetService.createStreetObj(cStreet, districtData),
                        HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Street: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/streets/{streetId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateStreet(@PathVariable("streetId") Integer streetId,
            @RequestBody CStreet cStreet) {
        try {
            Optional<CStreet> streetData = pStreetRepository.findById(streetId);
            if (streetData.isPresent()) {
                return new ResponseEntity<>(pStreetService.updateStreetObj(cStreet, streetData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Street: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Street NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/streets/{streetId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteStreetById(@PathVariable Integer streetId) {
        try {
            pStreetRepository.deleteById(streetId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
