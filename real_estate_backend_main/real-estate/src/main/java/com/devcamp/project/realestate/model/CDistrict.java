package com.devcamp.project.realestate.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "district")
public class CDistrict {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    @ManyToOne
    private CProvince _province;

    @OneToMany(targetEntity = CWard.class, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "_district_id")
    private Set<CWard> wards;

    @OneToMany(targetEntity = CStreet.class, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "_district_id")
    private Set<CStreet> streets;

    @OneToMany(targetEntity = CProject.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "_district_id")
    @JsonIgnore
    private Set<CProject> projects;

    @OneToMany(targetEntity = CRealEstate.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "district_id")
    @JsonIgnore
    private Set<CRealEstate> realEstates;

    public CDistrict() {
    }

    public CDistrict(int id, String name, String prefix, CProvince _province, Set<CWard> wards) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this._province = _province;
        this.wards = wards;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince get_province() {
        return _province;
    }

    public void set_province(CProvince _province) {
        this._province = _province;
    }

    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }

    public Set<CStreet> getStreets() {
        return streets;
    }

    public void setStreets(Set<CStreet> streets) {
        this.streets = streets;
    }

    public Set<CProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<CProject> projects) {
        this.projects = projects;
    }

    public Set<CRealEstate> getRealEstates() {
        return realEstates;
    }

    public void setRealEstates(Set<CRealEstate> realEstates) {
        this.realEstates = realEstates;
    }

}
