package com.devcamp.project.realestate.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "master_layout")
public class CMasterLayout {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name", nullable = false, length = 1000)
    private String name;

    @Column(name = "description", length = 5000)
    private String description;

    @ManyToOne
    private CProject project;

    @Column(name = "acreage", columnDefinition="Decimal(12,2)")
    private Double acreage;
    
    @Column(name = "apartment_list", length = 1000)
    private String apartmentList;
    
    @Column(name = "photo", length = 5000)
    private String photo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create")
    private Date dateCreate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_update")
    private Date dateUpdate;

    public CMasterLayout() {
    }

    public CMasterLayout(int id, String name, String description, CProject project, Double acreage,
            String apartmentList, String photo, Date dateCreate, Date dateUpdate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.project = project;
        this.acreage = acreage;
        this.apartmentList = apartmentList;
        this.photo = photo;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CProject getProject() {
        return project;
    }


    public void setProject(CProject project) {
        this.project = project;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public String getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(String apartmentList) {
        this.apartmentList = apartmentList;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    



}
