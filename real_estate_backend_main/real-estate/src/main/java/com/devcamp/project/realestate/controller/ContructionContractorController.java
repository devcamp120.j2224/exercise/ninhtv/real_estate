package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.ConstructionContractorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ContructionContractorController {
    @Autowired
    IConstructionContractorRepository pConstructionContractorRepository;

    @Autowired
    ConstructionContractorService pConstructionContractorService;

    @GetMapping("/constructionContractors")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getConstructionContractorsList() {
        if (!pConstructionContractorService.getConstructionContractorsList().isEmpty()) {
            return new ResponseEntity<>(pConstructionContractorService.getConstructionContractorsList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/constructionContractors/{constructionContractorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getConstructionContractorById(@PathVariable Integer constructionContractorId) {
        if (pConstructionContractorRepository.findById(constructionContractorId).isPresent()) {
            return new ResponseEntity<>(pConstructionContractorRepository.findById(constructionContractorId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/constructionContractors")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createConstructionContractor(
            @RequestBody CConstructionContractor cConstructionContractor) {
        try {
            return new ResponseEntity<>(
                    pConstructionContractorService.createConstructionContractorObj(cConstructionContractor),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified ConstructionContractor: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/constructionContractors/{constructionContractorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateConstructionContractor(
            @PathVariable("constructionContractorId") Integer constructionContractorId,
            @RequestBody CConstructionContractor cConstructionContractor) {
        try {
            Optional<CConstructionContractor> ConstructionContractorData = pConstructionContractorRepository
                    .findById(constructionContractorId);
            if (ConstructionContractorData.isPresent()) {
                return new ResponseEntity<>(
                        pConstructionContractorService.updateConstructionContractorObj(cConstructionContractor,
                                ConstructionContractorData),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified ConstructionContractor: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/constructionContractors/{constructionContractorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteConstructionContractorById(
            @PathVariable("constructionContractorId") int constructionContractorId) {
        try {
            pConstructionContractorRepository.deleteConstructionContractorById(constructionContractorId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
