package com.devcamp.project.realestate.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.*;
import org.springframework.stereotype.Repository;

@Repository
public interface IStreetRepository extends JpaRepository<CStreet, Integer> {
    
    @Query(value = "SELECT * FROM `street` LIMIT 100 OFFSET 0 ;", nativeQuery = true)
    List<CStreet> getStreetList();

    @Query(value = "SELECT * FROM `street` WHERE `_district_id` = :districtId ;", nativeQuery = true)
    List<CStreet> getStreetListByDistrictId(@Param("districtId") Integer districtId);
}
