package com.devcamp.project.realestate.model;

import javax.persistence.*;

@Entity
@Table(name = "locations")
public class CLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;

    @Column(name = "Latitude", nullable = false)
    private float latitude = 0;

    @Column(name = "Longitude", nullable = false)
    private float longitude = 0;

    public CLocation() {
    }

    public CLocation(int iD, float latitude, float longitude) {
        this.ID = iD;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setID(int iD) {
        this.ID = iD;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

}
