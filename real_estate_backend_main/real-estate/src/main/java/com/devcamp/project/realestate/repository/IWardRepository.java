package com.devcamp.project.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.*;
import org.springframework.stereotype.Repository;

@Repository
public interface IWardRepository extends JpaRepository<CWard, Integer> {

    @Query(value = "SELECT * FROM `ward` ;", nativeQuery = true)
    List<CWard> getWardList();

    @Query(value = "SELECT * FROM `ward` WHERE `_district_id` = :districtId ;", nativeQuery = true)
    List<CWard> getWardListByDistrictId(@Param("districtId") Integer districtId);
}
