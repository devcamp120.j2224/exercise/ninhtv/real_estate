package com.devcamp.project.realestate.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "street")
public class CStreet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    @ManyToOne
    @JsonIgnore
    private CProvince _province;

    @ManyToOne
    private CDistrict _district;

    @OneToMany(targetEntity = CProject.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "_street_id")
    @JsonIgnore
    private Set<CProject> projects;

    @OneToMany(targetEntity = CRealEstate.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "street_id")
    @JsonIgnore
    private Set<CRealEstate> realEstates;

    public CStreet() {
    }

    public CStreet(int id, String name, String prefix, CProvince _province, CDistrict _district) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this._province = _province;
        this._district = _district;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince get_province() {
        return _province;
    }

    public void set_province(CProvince _province) {
        this._province = _province;
    }

    public CDistrict get_district() {
        return _district;
    }

    public void set_district(CDistrict _district) {
        this._district = _district;
    }

    public Set<CProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<CProject> projects) {
        this.projects = projects;
    }

    public Set<CRealEstate> getRealEstates() {
        return realEstates;
    }

    public void setRealEstates(Set<CRealEstate> realEstates) {
        this.realEstates = realEstates;
    }

}
