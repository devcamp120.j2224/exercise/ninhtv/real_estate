package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.ISubscriptionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionService {
    @Autowired
    ISubscriptionRepository pSubscriptionRepository;

    public List<CSubscription> getSubscriptionList() {
        List<CSubscription> subscriptionList = new ArrayList<CSubscription>();
        pSubscriptionRepository.findAll().forEach(subscriptionList::add);
        return subscriptionList;
    }

    public Object createSubscriptionObj(CSubscription cSubscription) {

        CSubscription newSubscription = new CSubscription();
        newSubscription.setUser(cSubscription.getUser());
        newSubscription.setEndpoint(cSubscription.getEndpoint());
        newSubscription.setPublickey(cSubscription.getPublickey());
        newSubscription.setAuthenticationtoken(cSubscription.getAuthenticationtoken());
        newSubscription.setContentencoding(cSubscription.getContentencoding());

        CSubscription savedSubscription = pSubscriptionRepository.save(newSubscription);
        return savedSubscription;
    }

    public Object updateSubscriptionObj(CSubscription cSubscription, Optional<CSubscription> cSubscriptionData) {

        CSubscription newSubscription = cSubscriptionData.get();
        newSubscription.setUser(cSubscription.getUser());
        newSubscription.setEndpoint(cSubscription.getEndpoint());
        newSubscription.setPublickey(cSubscription.getPublickey());
        newSubscription.setAuthenticationtoken(cSubscription.getAuthenticationtoken());
        newSubscription.setContentencoding(cSubscription.getContentencoding());

        CSubscription savedSubscription = pSubscriptionRepository.save(newSubscription);
        return savedSubscription;
    }
}
